let fs = require("fs");
let _ = require('lodash');

let words = fs.readFileSync("pan_tadeusz.txt","utf8");

let arrayOfWords = _.words(_.lowerCase(words));

let arrayOfUniqWords = _.uniq(arrayOfWords);

let stringOfWords = _.join(arrayOfUniqWords, '\n');

fs.writeFileSync('word_list.txt', stringOfWords)

console.log('word_list done!')