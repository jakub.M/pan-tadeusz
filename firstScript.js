let fs = require("fs");
let _ = require('lodash');
let crypto = require('crypto');

const encryptMD5 = word => crypto.createHash('md5').update(word).digest("hex")

let words = fs.readFileSync(process.argv[2], "utf8");

let arrayOfWords = _.words(words);

let arrayOfWordsWithCrypto = arrayOfWords.map( word => `${_.padEnd(word, 13)}${encryptMD5(word)}`)

let stringOfWordsWithCrypto = _.join(arrayOfWordsWithCrypto, '\n');

fs.writeFileSync('rainbow_word_list.txt', stringOfWordsWithCrypto)

console.log('rainbow_word_list done!')

