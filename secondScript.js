let fs = require("fs");
let _ = require('lodash');

let wordsWithCrypto = fs.readFileSync(process.argv[2], "utf8");

let arrayOfWordsWithCrypto = _.split(wordsWithCrypto, '\n')

let passwordWithCrypto = arrayOfWordsWithCrypto.find(item => item.includes(process.argv[3]));

let extractedPassword = _.words(passwordWithCrypto)[0];

fs.writeFileSync('result.txt', `Password is : ${extractedPassword}`);

console.log('Password done!');